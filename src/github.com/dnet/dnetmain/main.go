package main

import (
	"github.com/dnet/memberlist"
	"github.com/dnet/smudge"
)

func main() {

	// Uncomment below to disable function
	memberlist.MemberListMain()
	smudge.SmudgeMain()

}