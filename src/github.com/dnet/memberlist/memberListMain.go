package memberlist

import (
	"log"
	"github.com/hashicorp/memberlist"
	"time"
)

var TargetIP = "1.1.1.1" // IP Node to join

func MemberListMain() {
	// Code here...
	log.Println("MemberListMain() running...")
	log.Println("Start node.")

	config := memberlist.DefaultWANConfig()
	list, err := memberlist.Create(config)
	if err != nil {
		log.Println("Failed to create memberlist: " + err.Error())
	}

	// Join
	clusterCount, err := list.Join([]string{TargetIP})
	log.Println("Joing cluster of size", string(clusterCount))
	if err != nil {
		log.Println("Failed to join cluster: " + err.Error())
	}

	// Log members list every 2secs
	for {
		time.Sleep(time.Second * 2)
		log.Println("Members:")
		for _, member := range list.Members() {
			log.Printf("  %s %s\n", member.Name, member.Addr)
		}
	}
}
